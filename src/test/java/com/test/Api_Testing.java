package com.test;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

public class Api_Testing {
  @Test
	  public void getMultipleUser() {
		  
		  baseURI="https://gorest.co.in";
		  given().get("/public/v2/users").then().statusCode(200).log().all();
		  
	  }  
  @Test
  public void postUser() {
  	  
  	  JSONObject request=new JSONObject();
  	  request.put("name","Tenali Ramakrishnan");
  	  request.put("gender","male");
  	  request.put("email","tenali.ramakrishna@1911ce.com");
  	  request.put("status","active");
  	  baseURI="https://gorest.co.in";
  	  given().log().all().contentType("application/json").header("authorization","Bearer bdfffd01bd20489d9d0689a4ed4236f66afd6992ea08089c6e6939214f615afc").body(request.toJSONString()).when().post("/public/v2/users").then().statusCode(201);
  	
       
  }
@Test
public void getSingleUser() {
	  
	  baseURI="https://gorest.co.in";
	  given().get("/public/v2/users/515").then().statusCode(200).log().all();
	  
}

@Test
public void putUser() {
	  
	  JSONObject request=new JSONObject();
	  request.put("name","Tenali Ramakrishna");
	  request.put("gender","male");
	  request.put("email","tenali.ramakrishna@giiglece.com");
	  request.put("status","active");
	  baseURI="https://gorest.co.in";
	  given().log().all().contentType("application/json").header("authorization","Bearer bdfffd01bd20489d9d0689a4ed4236f66afd6992ea08089c6e6939214f615afc").body(request.toJSONString()).when().put("/public/v2/users/193929").then().statusCode(200);
		
}
@Test
public void delete() {
	baseURI="https://gorest.co.in";
	 given().log().all().contentType("application/json").header("authorization","Bearer bdfffd01bd20489d9d0689a4ed4236f66afd6992ea08089c6e6939214f615afc").delete("/public/v2/users/193929").then().statusCode(204);
}

}
